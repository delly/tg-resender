from peewee import *
import datetime
import logging


db = SqliteDatabase('/storage/tg-resender/db/resender.db')
class BaseModel(Model):
    class Meta:
        database = db
class fwd(BaseModel):
    channel_post_id = CharField(unique=True)


db.create_tables([fwd])


# def create_tables():
#     with db:
#         db.create_tables([Forward])
