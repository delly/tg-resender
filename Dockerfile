FROM python:slim

WORKDIR /tg-resender

COPY . .

RUN pip install --no-cache-dir -r requirements.txt && pip install --upgrade https://github.com/LonamiWebs/Telethon/archive/master.zip