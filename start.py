from telethon import TelegramClient, events
from database import *
import os
from os import environ
from dotenv import load_dotenv
import logging
from utils import parse_string
import time



load_dotenv(dotenv_path='/storage/tg-resender/conf/.env')
logging.basicConfig(format='[%(levelname) 5s/%(asctime)s] %(name)s: %(message)s',
                    level=logging.WARNING)


api_id = os.getenv("API_ID")
api_hash = os.getenv("API_HASH")
CHANNELS_MAPPING = parse_string(environ.get('CHANNELS_MAPPING', ''))
SOURCE_CHANNELS = list(CHANNELS_MAPPING.keys())


client = TelegramClient('tg-resender', api_id, api_hash)

print ('Started')

@client.on(events.NewMessage(chats=SOURCE_CHANNELS))
async def handler_new_message(event):
    try:
        mirror_channel = CHANNELS_MAPPING.get(event.chat_id)
        if mirror_channel is None and len(mirror_channel) < 1:
            return
        for c in mirror_channel:
            # print(event.message)
            try:
                check_double_channel = event.message.fwd_from.from_id.channel_id
                check_double_id = event.message.fwd_from.channel_post
            except Exception:
                check_double_channel = event.message.peer_id.channel_id
                check_double_id = event.message.id
            check_double_channel_id = str(check_double_channel) + str(check_double_id)
            print(check_double_channel_id)
            try:
                check_double = fwd.get(fwd.channel_post_id == check_double_channel_id)
                print('double')
            except fwd.DoesNotExist:
                await client.forward_messages(c, event.message)
                post_in_db = fwd.create(channel_post_id=check_double_channel_id)
                time.sleep(5)
    except Exception as e:
        print(e)

    
if __name__ == '__main__':
    client.start()
    client.run_until_disconnected()